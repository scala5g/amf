import axios from 'axios';
import express, { response } from "express";
import bodyParser from 'body-parser';

const app = express()

app.use(bodyParser.urlencoded({ extended: true}))
//TODO mock database as an interface and see if the user can perform operations
function userIsAbleToPerformOperations(userid: string): boolean{
    return true
}

app.post("/sessionrequest", async (req, res) => {
    if(userIsAbleToPerformOperations(req.body.msisdn)){
        await axios.post('http://smf-service:8323/createpdu', {
            msisdn: req.body.msisdn
        }).then(smfresponse=>{
            const result = smfresponse.data
            console.log("response from smf: " + result)
            console.log(result)
            res.status(200)
            res.end("amf:"+result)
        }
        ).catch( error => {
            console.log("amf error on axios")
            res.status(503)
            res.end("amf:error on smf server")
        })
    }else{
        res.end(403)
    }
})

app.listen(7325, () => {
    console.log("Init AMF")
})